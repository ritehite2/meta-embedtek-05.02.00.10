FILESEXTRAPATHS_prepend := "${THISDIR}/patches:"

#The space at the end of the quote fixes an issue with playing nice with another layer
#Only needed if a second entry exists
SRC_URI += "file://0001-Changes-for-door-controller-boot.patch"

