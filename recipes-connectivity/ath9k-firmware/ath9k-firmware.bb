DESCRIPTION = "Installs ATH9K Firmware"
DEPENDS = ""
LICENSE = "BSD-4-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=1b33c9f4d17bc4d457bdb23727046837"
S = "${WORKDIR}"

PACKAGES = "${PN}"

SRC_URI += "file://htc_9271-1.4.0.fw "
SRC_URI += "file://LICENSE "

do_install(){
    install -d ${D}/lib/firmware/ath9k_htc
    install ${WORKDIR}/htc_9271-1.4.0.fw ${D}/lib/firmware/ath9k_htc
}

FILES_${PN} += "/lib/firmware/ath9k_htc/"

