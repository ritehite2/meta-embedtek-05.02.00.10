#configure wpa_supplicant to use openssl for encryption to avoid licensing issues
PACKAGECONFIG_remove="gnutls"
PACKAGECONFIG_append=" openssl"
