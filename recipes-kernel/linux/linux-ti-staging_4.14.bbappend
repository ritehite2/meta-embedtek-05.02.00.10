FILESEXTRAPATHS_prepend := "${THISDIR}/devicetree-patches:"
FILESEXTRAPATHS_prepend := "${THISDIR}/kernel-fragments:"

#The space at the end of the quote fixes an issue with playing nice with another layer
SRC_URI += "file://0001-Configure-device-tree-for-door-controller-board.patch "
SRC_URI += "file://ATH9K_Support.cfg "
SRC_URI += "file://OMAP_Serial_Support.cfg "
SRC_URI += "file://PL2303_Support.cfg "
SRC_URI += "file://RTC_TPS65910_Support.cfg "

#meta-ti layer is using these fragments, not the standard yocto method
KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/ATH9K_Support.cfg \
				${WORKDIR}/OMAP_Serial_Support.cfg \
				${WORKDIR}/PL2303_Support.cfg \
				${WORKDIR}/RTC_TPS65910_Support.cfg"

